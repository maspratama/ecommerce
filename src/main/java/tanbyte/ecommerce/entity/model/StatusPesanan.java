package tanbyte.ecommerce.entity.model;

public enum StatusPesanan {
    DRAFT, PEMBAYARAN, PACKING, PENGIRIMAN, SAMPAITUJUAN
}
