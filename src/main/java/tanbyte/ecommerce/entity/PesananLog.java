package tanbyte.ecommerce.entity;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;

@Data
@Entity
@Table(name = "tbl_pesanan_log")
public class PesananLog implements Serializable{

    @Id
    private String Id;
    
    @JoinColumn
    @ManyToOne
    private Pesanan pesanan;

    @JoinColumn
    @ManyToOne
    private Pengguna pengguna;

    private Integer logType;
    private String logMessage;

    @Temporal(TemporalType.TIMESTAMP)
    private Date waktu;
}
