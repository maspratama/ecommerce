package tanbyte.ecommerce.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import tanbyte.ecommerce.entity.model.StatusPesanan;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;

@Data
@Entity
@Table(name = "tbl_pesanan")
public class Pesanan implements Serializable{
    
    @Id
    private String Id;
    private String reffNumber;

    @Temporal(TemporalType.DATE) //untuk menandai waktu saja / tanggal saja
    private Date tanggal;

    @JoinColumn
    @ManyToOne
    private Pengguna pengguna;
    
    private String alamatPengiriman;
    private BigDecimal jumlah;
    private BigDecimal ongkir;
    private BigDecimal total;

    @Enumerated(EnumType.STRING)
    private StatusPesanan statusPesanan;

    @Temporal(TemporalType.TIMESTAMP)
    private Date waktuPesan;
    
}
