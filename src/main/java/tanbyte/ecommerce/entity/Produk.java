package tanbyte.ecommerce.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "tbl_produk")
public class Produk implements Serializable {
    
    @Id
    private String Id;
    private String nama;
    private String deskripsi;
    private String gambar;

    @JoinColumn
    @ManyToOne
    private Kategori kategoriId;

    private BigDecimal harga;
    private Double stok;


}
