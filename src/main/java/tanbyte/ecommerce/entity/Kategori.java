package tanbyte.ecommerce.entity;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "tbl_kategori")
public class Kategori implements Serializable {

    @Id
    private String Id;
    private String nama;
}