package tanbyte.ecommerce.entity;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "tbl_pengguna")
public class Pengguna implements Serializable{
    
    @Id
    private String Id;
    private String password;
    private String nama;
    private String alamat;
    private String email;
    private String noHp;
    private String roles;
    private Boolean isActived;
}
