package tanbyte.ecommerce.entity;

import java.math.BigDecimal;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "tbl_pesanan_item")
public class PesananItem {
    
    @Id
    private String Id;

    @JoinColumn
    @ManyToOne
    private Produk produk;
    private String deskripsi;
    private Double kuantitas;
    private BigDecimal harga;
    private BigDecimal jumlah;
}
